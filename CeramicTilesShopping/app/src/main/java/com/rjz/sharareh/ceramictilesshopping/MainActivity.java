package com.rjz.sharareh.ceramictilesshopping;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btn_product, btn_ideas, btn_installation, btn_exit, btn_login, btn_contactus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_exit = (Button) findViewById(R.id.main_exit);
        btn_ideas = (Button) findViewById(R.id.main_ideas);
        btn_installation = (Button) findViewById(R.id.main_installation);
        btn_login = (Button) findViewById(R.id.main_login);
        btn_product = (Button) findViewById(R.id.main_product);
        btn_contactus = (Button) findViewById(R.id.main_contactus);

        //------------Exit Button------------------------------------------------
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //---------Product Button-------------------------------------------------
        btn_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent product = new Intent(MainActivity.this, Product.class);
                startActivity(product);
            }
        });
        //----------Contact us----------------------------------------------------
        btn_contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactus = new Intent(MainActivity.this, Contactus.class);
                startActivity(contactus);
            }
        });
        //---------- ideas----------------------------------------------------
        btn_ideas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ideas = new Intent(MainActivity.this, Ideas.class);
                startActivity(ideas);
            }
        });
        //----------installation--------------------------------------------------
        btn_installation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent installatio = new Intent(MainActivity.this, Installation.class);
                startActivity(installatio);
            }
        });
        //---------- login----------------------------------------------------
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(MainActivity.this, Login.class);
                startActivity(login);
            }
        });

    }
}

